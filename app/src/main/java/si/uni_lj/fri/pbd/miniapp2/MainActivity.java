package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private MediaPlayerService mediaPlayerService;

    // variable that indicates whether the MediaPlayerService is connected
    private boolean mediaPlayerServiceBound;

    private TextView songInfo, duration;

    // variable that indicates whether the broadcast is registered
    private boolean isRestricted;

    //  define a MediaPlayerService connection and disconnection
    private ServiceConnection mediaPlayerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MediaPlayerService.MediaPlayerServiceLocalBinder binder = (MediaPlayerService.MediaPlayerServiceLocalBinder) iBinder;
            mediaPlayerService = binder.getService();
            mediaPlayerServiceBound = true;
            // Update we play the song at least once update the UI
            if (mediaPlayerService.isPlaying() || mediaPlayerService.isPaused() || mediaPlayerService.isStopped()) {
                mediaPlayerService.updateUI();
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mediaPlayerServiceBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get the textViews
        songInfo = (TextView) findViewById(R.id.song_info);
        duration = (TextView) findViewById(R.id.duration);
    }

    // start the service
    @Override
    protected void onStart() {
        super.onStart();
        // Start the MediaPlayerService
        Intent i = new Intent(this, MediaPlayerService.class);
        i.setAction(MediaPlayerService.ACTION_START);
        startService(i);
        // bind the service
        bindService(i, mediaPlayerServiceConnection, 0);
        // broadcast
        IntentFilter filter = new IntentFilter("Song and time");
        registerReceiver(broadcastReceiver, filter);
        isRestricted = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        // if the Service is bound, unbind it
        if (mediaPlayerServiceBound) {
            unbindService(mediaPlayerServiceConnection);
        }
        if (isRestricted) {
            unregisterReceiver(broadcastReceiver);
            isRestricted = false;
        }
    }

    // if the user click on play button, play music
    public void play(View view) {
        // check if the service is bound
        if (mediaPlayerServiceBound) {
            // play the song from the service
            mediaPlayerService.start();
            Toast.makeText(getApplicationContext(), "Start Player", Toast.LENGTH_SHORT).show();
        }
    }

    // if the user click on pause button, pause the music
    public void pause(View view) {
        // check if the service is bound
        if (mediaPlayerServiceBound) {
            // pause the music from the service
            mediaPlayerService.pause();
            Toast.makeText(getApplicationContext(), "Pause Player", Toast.LENGTH_SHORT).show();
        }
    }

    // if the user click on stop button, stop the music
    public void stop(View view) {
        // check if the service is bound
        if (mediaPlayerServiceBound) {
            // stop the music from the service
            mediaPlayerService.stop();
            Toast.makeText(getApplicationContext(), "Stop Player", Toast.LENGTH_SHORT).show();
        }
    }

    // if the user click on exit button, stop everything and exit the app
    public void exit(View view) {
        // check if the service is bound
        if (mediaPlayerServiceBound) {
            // exit from the service
            mediaPlayerService.exit();
        }
    }

    // get the data from the MediaPlayerService
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals("Song and time")){
                String song = intent.getExtras().getString("song");
                String time = intent.getExtras().getString("time");
                if (song.equals("no song")) {
                    songInfo.setText("(song info)");
                    duration.setText("(duration)");
                }
                else {
                    songInfo.setText(song);
                    duration.setText(time);
                }
            }
        }
    };

    // if the user click on GesturesOn button, activate the gestures
    public void gesturesOn (View view) {
        // check if the MediaPlayerService is bound, connect the AccelerationService
        if (mediaPlayerServiceBound) {
            Toast.makeText(getApplicationContext(), "Gesture commands activated", Toast.LENGTH_SHORT).show();
            mediaPlayerService.gesturesOn();
        }
    }

    // if the user click on GesturesOff button, deactivate the gestures
    public void gesturesOff (View view) {
        // check if the service is bound
        if (mediaPlayerServiceBound) {
            Toast.makeText(getApplicationContext(), "Gesture commands deactivated", Toast.LENGTH_SHORT).show();
            mediaPlayerService.gesturesOff();
        }
    }
}

