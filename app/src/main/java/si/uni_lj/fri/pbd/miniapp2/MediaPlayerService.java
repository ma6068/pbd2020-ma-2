package si.uni_lj.fri.pbd.miniapp2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;


public class MediaPlayerService extends Service {

    public static final String ACTION_START = "start_service";
    public static final String ACTION_PLAY_MUSIC = "play_music";
    public static final String ACTION_PAUSE_MUSIC = "pause_music";
    public static final String ACTION_STOP_MUSIC = "stop_music";
    public static final String ACTION_EXIT = "exit_service";


    private final IBinder serviceBinder = new MediaPlayerServiceLocalBinder();

    private static final int NOTIFICATION_ID = 1;
    private static final String channelID = "background_timer";

    private MediaPlayer mediaPlayer;

    // shows the state
    private boolean isPlaying;
    private boolean isPaused;
    private boolean isStopped;

    // shows which song is playing / should play next
    private int song;

    private NotificationCompat.Builder builder;
    private Handler handler;

    // AccelerationService object
    private AccelerationService accelerationService;

    // variable that indicates whether the AccelerationService is connected
    private boolean accelerationServiceBound;

    // variable that indicates whether the broadcast is registered
    private boolean isRegistered;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

    public class MediaPlayerServiceLocalBinder extends Binder {
        MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

    @Override
    public void onCreate() {
        isPlaying = false;
        isPaused = false;
        isStopped = false;
        isRegistered = false;
        // create notification channel
        createNotificationChannel();

        // create notification
        updateUI();

    }

    public void updateUI() {
        startForeground(NOTIFICATION_ID, createNotification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // check the intent action and if equal to ACTION_STOP, stop the foreground service
        if (intent.getAction().equals(ACTION_PLAY_MUSIC)) {
            start();
        }
        else if (intent.getAction().equals(ACTION_PAUSE_MUSIC)) {
            pause();
        }
        else if (intent.getAction().equals(ACTION_STOP_MUSIC)) {
            stop();
        }
        else if (intent.getAction().equals(ACTION_EXIT)) {
            exit();
        }

        // handler
        handler = new Handler();

        // bound the AccelerationService if is not
        if (!accelerationServiceBound) {
            Intent i = new Intent(MediaPlayerService.this, AccelerationService.class);
            bindService(i, accelerationServiceConnection, Context.BIND_AUTO_CREATE);
        }

        return Service.START_STICKY;
    }

    public void start() {
        // user clicks play for the first time
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.song1);
            mediaPlayer.start();
            song = 1;
        }
        // user clicks play, to start the paused / stopped music
        else if (isPaused() || isStopped()) {
            mediaPlayer.start();
        }
        isPlaying = true;
        isPaused = false;
        isStopped = false;
        startTimer();

        // if the song end => start the next one
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stop();
                start();
            }
        });
    }

    public void pause() {
        // pause the song if is playing
        if (mediaPlayer != null && isPlaying()) {
            mediaPlayer.pause();
            isPaused = true;
            isPlaying = false;
            isStopped = false;
            stopTimer();
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            isStopped = true;
            isPlaying = false;
            isPaused = false;
            // go to the next song (change the song on next play-click)
            if (song == 1) {
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.song2);
                song = 2;
            }
            else if (song == 2) {
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.song3);
                song = 3;
            }
            else if (song == 3) {
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.song1);
                song = 1;
            }
            stopTimer();
        }
    }

    public void exit() {
        // stop the music if it't playing
        if (isPlaying()) {
            mediaPlayer.stop();
        }
        // stop the MediaPlayerService (we already know that is bound)
        Intent intent = new Intent(MediaPlayerService.this, MediaPlayerService.class);
        stopService(intent);
        // stop the AccelerationService if is bound
        if (accelerationServiceBound) {
            // if the gestures are on => unregister the listener
            accelerationService.gesturesOff();
            // stop the AccelerationService
            stopService(new Intent(this, AccelerationService.class));
        }
        // exit the app
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    // get the song order
    public int getSongNumber() {
        return song;
    }

    // get the song state
    public boolean isPlaying() {
        return isPlaying;
    }
    public boolean isPaused() {
        return isPaused;
    }
    public boolean isStopped() {
        return isStopped;
    }

    // get the String in form playing or paused time / time duration
    public String getTime() {
        return timeDuration(mediaPlayer.getCurrentPosition()) + "/" + timeDuration(mediaPlayer.getDuration());
    }

    // time duration in sec, min, h (from int to String)
    public String timeDuration (int d) {
        String time = "";
        int seconds = d / 1000 % 60;
        int minutes = d / 1000 / 60;
        int hours = d / 1000 / 60 / 60;
        time += Long.toString(hours) + ":";
        if (minutes < 10) {
            time += "0" + minutes;
        }
        else {
            time += Long.toString(minutes);
        }
        time += ":";
        if (seconds < 10) {
            time += "0" + seconds;
        }
        else {
            time += Long.toString(seconds);
        }
        return time;
    }

    // get the song info that is playing/paused
    public String getSongInfo() {
        if (getSongNumber() == 1) {
            return "1.Sebastian Ingrosso & Alesso ft. Ryan Tedder -- Calling";
        }
        else if (getSongNumber() == 2) {
            return "2.The Chainsmokers - Don't Let Me Down";
        }
        return "3.Fifth Harmony - Work from Home ft. Ty Dolla $ign";
    }

    // Creates a notification for placing the service into the foreground
    @SuppressLint("RestrictedApi")
    private Notification createNotification() {

        // play intent
        Intent playIntent = new Intent(this, MediaPlayerService.class);
        playIntent.setAction(ACTION_PLAY_MUSIC);
        PendingIntent playPendingIntent = PendingIntent.getService(this, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // pause intent
        Intent pauseIntent = new Intent(this, MediaPlayerService.class);
        pauseIntent.setAction(ACTION_PAUSE_MUSIC);
        PendingIntent pausePendingIntent = PendingIntent.getService(this, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        // stop intent
        Intent stopIntent = new Intent(this, MediaPlayerService.class);
        stopIntent.setAction(ACTION_STOP_MUSIC);
        PendingIntent stopPendingIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // exit intent
        Intent exitIntent = new Intent(this, MediaPlayerService.class);
        exitIntent.setAction(ACTION_EXIT);
        PendingIntent exitPendingIntent = PendingIntent.getService(this, 0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // create builder for the first time (add the action buttons)
        if (!isPlaying() && !isPaused() && !isStopped()) {
            builder = new NotificationCompat.Builder(this, channelID);
            builder.setSmallIcon(R.drawable.logo).setChannelId(channelID);
            builder.addAction(R.drawable.ic_play, "PLAY", playPendingIntent);
            builder.addAction(R.drawable.ic_stop, "STOP", stopPendingIntent);
            builder.addAction(R.drawable.ic_exit, "EXIT", exitPendingIntent);
        }

        // if the music is playing
        if (isPlaying()) {
            builder.mActions.clear();
            // set the song and the time
            builder.setContentTitle(getSongInfo());
            builder.setContentText(getTime());
            builder.addAction(R.drawable.ic_pause, "PAUSE", pausePendingIntent);
            builder.addAction(R.drawable.ic_stop, "STOP", stopPendingIntent);
            builder.addAction(R.drawable.ic_exit, "EXIT", exitPendingIntent);
            // send the data to MainActivity
            Intent intent = new Intent("Song and time");
            intent.putExtra("song", getSongInfo());
            intent.putExtra("time", getTime());
            sendBroadcast(intent);
        }

        // if the song is paused change the button to play
        if (isPaused()) {
            builder.mActions.clear();
            // set the title
            builder.setContentTitle(getSongInfo());
            builder.addAction(R.drawable.ic_play, "PLAY", playPendingIntent);
            builder.addAction(R.drawable.ic_stop, "STOP", stopPendingIntent);
            builder.addAction(R.drawable.ic_exit, "EXIT", exitPendingIntent);
            // send the data to MainActivity
            Intent intent = new Intent("Song and time");
            intent.putExtra("song", getSongInfo());
            intent.putExtra("time", getTime());
            sendBroadcast(intent);
        }

        // if the music is stopped hide the title and the time
        if (isStopped()) {
            builder.mActions.clear();
            builder.addAction(R.drawable.ic_play, "PLAY", playPendingIntent);
            builder.addAction(R.drawable.ic_stop, "STOP", stopPendingIntent);
            builder.addAction(R.drawable.ic_exit, "EXIT", exitPendingIntent);
            builder.setContentTitle("");
            builder.setContentText("");
            // send the data to MainActivity (with no song and no time, we will know that the player is stopped)
            Intent intent = new Intent("Song and time");
            intent.putExtra("song", "no song");
            intent.putExtra("time", "no time");
            sendBroadcast(intent);
        }

        return builder.build();
    }

    // Creating a notification channel for the foreground service
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        else {
            NotificationChannel channel = new NotificationChannel(MediaPlayerService.channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channel_desc));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }

    public void startTimer() {
        runnable.run();
    }

    public void stopTimer() {
        handler.removeCallbacks(runnable);
        // refresh the songinfo and timer
        updateUI();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // refresh the timer every second
            updateUI();
            handler.postDelayed(this, 1000);
        }
    };

    public void gesturesOn() {
        if (accelerationServiceBound) {
            accelerationService.gesturesOn();
            // register a broadcast receiver
            IntentFilter filter = new IntentFilter("SensorChanged");
            registerReceiver(broadcastReceiver, filter);
            isRegistered = true;
        }
    }

    public void gesturesOff() {
        if (accelerationServiceBound) {
            accelerationService.gesturesOff();
            if (isRegistered) {
                unregisterReceiver(broadcastReceiver);
                isRegistered = false;
            }
        }
    }

    //  define a AccelerationService connection and disconnection
    private ServiceConnection accelerationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AccelerationService.AccelerationServiceLocalBinder binder = (AccelerationService.AccelerationServiceLocalBinder) iBinder;
            accelerationService = binder.getService();
            accelerationServiceBound = true;
            // TODO: maybe should add something here
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            accelerationServiceBound = false;
        }
    };

    // get the data from the AccelerationService
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals("SensorChanged")){
                String change = intent.getExtras().getString("change");
                if (change.equals("pause")) {
                    pause();
                }
                else if (change.equals("play")) {
                    start();
                }
            }
        }
    };
}
